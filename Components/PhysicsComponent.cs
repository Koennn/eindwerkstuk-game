﻿using System;
using UnityEngine;

namespace Koen
{
    public class PhysicsComponent : MonoBehaviour
    {
        CharacterController cc;
        public float VerticalVelocity;
        public float JumpHeight;
        
        /// <summary>
        /// Initialize the PhysicsComponent
        /// </summary>
        void Start()
        {
            cc = GetComponent<CharacterController>();
        }

        /// <summary>
        /// Handle gravity and movement.
        /// Gets called every frame.
        /// </summary>
        void Update()
        {
            //Make sure the JumpHeight is set.
            if (JumpHeight <= 0)
            {
                JumpHeight = Game.Controls.JumpHeight;
            }

            //Check if the CharacterController is on solid ground.
            if (!cc.isGrounded)
            {
                //Apply gravity to the vertical velocity.
                VerticalVelocity += Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                //Floor collider push fix.
                VerticalVelocity = VerticalVelocity < 1 ? -0.0001F : VerticalVelocity;
            }
            //Move the CharacterController in the right direction.
            cc.Move(new Vector3(0, VerticalVelocity, 0) * Time.deltaTime);
        }

        /// <summary>
        /// Move the CharacterController in a certain direction.
        /// </summary>
        /// <param name="Direction">Direction to move in</param>
        public void Move(Vector3 Direction)
        {
            //Move the CharacterController in the right direction.
            cc.Move(Direction * Time.deltaTime);
        }

        /// <summary>
        /// Make the CharacterController jump in the air.
        /// </summary>
        public void Jump()
        {
            VerticalVelocity = JumpHeight;
            cc.Move(new Vector3(0, VerticalVelocity, 0) * Time.deltaTime);
        }
    }
}
