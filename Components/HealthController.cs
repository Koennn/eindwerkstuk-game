﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Health management class.
    /// Apply to any GameObject to give it Health.
    /// </summary>
    public class HealthController : MonoBehaviour
    {
        public int MaxHealth;
        private int Health;

        /// <summary>
        /// Setup the Heatlh object.
        /// Gets called on the start of the game.
        /// </summary>
        void Start()
        {
            Health = MaxHealth;
        }

        /// <summary>
        /// Set the Health of the GameObject to a certain amount.
        /// </summary>
        /// <param name="Amount">Amount of health</param>
        public void SetHealth(int Amount)
        {
            Health = Amount;
        }

        /// <summary>
        /// Get the Health of the GameObject
        /// </summary>
        /// <returns>int Health</returns>
        public int GetHealth()
        {
            return Health;
        }

        /// <summary>
        /// Apply a certain amount of damage directly to the GameObject.
        /// </summary>
        /// <param name="Amount">Amount of damage to apply</param>
        public void ApplyDamage(int Amount)
        {
            SetHealth(Health - Amount);
        }
    }
}
