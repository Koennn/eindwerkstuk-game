﻿using System;

namespace Koen
{
    /// <summary>
    /// General Event class for all UncheckedEvents.
    /// An UncheckedEvent will ALWAYS execute its code.
    /// An UncheckedEvent will call the Cancelled method if the Event is denied by the server.
    /// </summary>
    public abstract class UncheckedEvent : Event
    {
        /// <summary>
        /// Initialize the UncheckedEvent and it's base class.
        /// </summary>
        /// <param name="EventName">Name of the event</param>
        public UncheckedEvent(string EventName) : base(EventName)
        {
            Logger.Debug("Loading new event '" + EventName + "'");
        }

        /// <summary>
        /// Run the Cancel code if the Event is cancelled by the server.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        public abstract void Cancelled(object[] Args);

        /// <summary>
        /// Call the Event on all it's listeners.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        [Obsolete("Server check not implemented yet, directly executing the Event's code!")]
        public override void Call(object[] Args)
        {
            //TEMP
            Check(true, Args);
        }

        /// <summary>
        /// Run the Check code after the Event is returned by the server.
        /// </summary>
        /// <param name="Response">Response from the server</param>
        /// <param name="Args">Event arguments</param>
        public override void Check(bool Response, object[] Args)
        {
            if (!Response)
            {
                Cancelled(Args);
            }
        }
    }
}
