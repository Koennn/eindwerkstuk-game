﻿using System;

namespace Koen
{
    /// <summary>
    /// General Event class.
    /// Base class of all Event classes
    /// </summary>
    public abstract class Event
    {
        /// <summary>
        /// Name of the Event.
        /// </summary>
        public string EventName;

        /// <summary>
        /// Initialize the Event and set its name.
        /// </summary>
        /// <param name="EventName">Name of the Event</param>
        public Event(string EventName)
        {
            this.EventName = EventName;
        }

        /// <summary>
        /// Call the Event on all it's listeners.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        public abstract void Call(object[] Args);

        /// <summary>
        /// Call the check code.
        /// Gets called when the server accepts or denies the Event.
        /// </summary>
        /// <param name="Response">Response of the server</param>
        /// <param name="Args">Event arguments</param>
        public abstract void Check(bool Response, object[] Args);

        /// <summary>
        /// Execute the Event's code.
        /// Can only be called from inherited class members.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        protected abstract void Execute(object[] Args);
    }
}
