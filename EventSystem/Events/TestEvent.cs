﻿using System;

namespace Koen
{
    /// <summary>
    /// Simple Debug Event to test the EventSystem
    /// Has no use at all!
    /// </summary>
    class TestEvent : CheckedEvent
    {
        /// <summary>
        /// Initialize the TestEvent and it's base classes.
        /// </summary>
        public TestEvent() : base("TestEvent")
        {
            Logger.Debug("Created TestEvent");
        }

        /// <summary>
        /// Execute the TestEvent.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        protected override void Execute(object[] Args)
        {
            Logger.Debug("Event executed");
        }
    }
}
