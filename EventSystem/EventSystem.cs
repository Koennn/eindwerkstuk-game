﻿using System;
using System.Collections.Generic;

namespace Koen
{
    public class EventSystem
    {
        List<Event> RegisteredEvents;

        public EventSystem()
        {
            RegisteredEvents = new List<Event>();
        }

        public void RegisterEvent(Event Event)
        {
            RegisteredEvents.Add(Event);
            Logger.Debug("Registered event '" + Event.EventName + "'");
        }

        public void CallEvent(string Name, Object[] Args)
        {
            foreach (Event Event in RegisteredEvents)
            {
                if (Event.EventName.Equals(Name))
                {
                    Event.Call(Args);
                    return;
                }
            }
            throw new ArgumentException("Unknown event '" + Name + "'");
        }
    }
}
