﻿using System;

namespace Koen
{
    /// <summary>
    /// General Event class for all CheckedEvents.
    /// A CheckedEvent will ONLY execute its code when the event is accepted by the server.
    /// </summary>
    public abstract class CheckedEvent : Event
    {
        /// <summary>
        /// Initialize the CheckedEvent and it's base class.
        /// </summary>
        /// <param name="EventName">Name of the Event</param>
        public CheckedEvent(string EventName) : base(EventName)
        {
            Logger.Debug("Loading new event '" + EventName + "'");
        }

        /// <summary>
        /// Call the Event on all it's listeners.
        /// </summary>
        /// <param name="Args">Event arguments</param>
        [Obsolete("Server check not implemented yet, directly executing the Event's code!")]
        public override void Call(object[] Args)
        {
            //TEMP
            Check(true, Args);
        }

        /// <summary>
        /// Run the Check code after the Event is returned by the server.
        /// </summary>
        /// <param name="Response">Response from the server</param>
        /// <param name="Args">Event arguments</param>
        public override void Check(bool Response, object[] Args)
        {
            if (Response)
            {
                this.Call(Args);
            }
        }
    }
}
