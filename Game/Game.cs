﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Main class of the whole game.
    /// Used to store class instances and get certain objects.
    /// Does nothing on its own.
    /// </summary>
    public class Game : MonoBehaviour
    {
        //Static objects.
        public static GameTicker Ticker;
        public static Controls Controls;
        public static Player PlayerObject;
        public static EventSystem EventSys;

        //All the different Canvas objects.
        public static Canvas HUD;
        public static Canvas EscapeOverlay;
        public static Canvas QuitOverlay;

        /// <summary>
        /// Initialize all Game components.
        /// Gets called when the class enables.
        /// </summary>
        void Start()
        {
            //Disable the mouse cursor.
            MouseHelper.LockMouse();

            //Get the class objects.
            Ticker = GetComponent<GameTicker>();
            Controls = GetComponent<Controls>();

            //Initialize the EventSystem.
            EventSys = new EventSystem();

            //Get the Canvas objects.
            HUD = GameObject.Find("HUD").GetComponent<Canvas>();
            EscapeOverlay = GameObject.Find("Escape Overlay").GetComponent<Canvas>();

            //Disable the unused components.
            EscapeOverlay.gameObject.SetActive(false);

            //Start the Player object.
            PlayerObject = GameObject.Find("Player").GetComponent<Player>();
            PlayerObject.Start();
        }

        /// <summary>
        /// Close the whole program.
        /// </summary>
        public static void CloseGame()
        {
            Ticker.Enabled = false;
            Application.Quit();
        }
    }
}