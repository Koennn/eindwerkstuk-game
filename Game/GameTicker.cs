﻿using System.Threading;
using UnityEngine;
using System.Collections.Generic;

namespace Koen
{
    /// <summary>
    /// Simple class which will execute given tasks every tick.
    /// A tick is called 10 times every second.
    /// </summary>
    public class GameTicker : MonoBehaviour
    {
        List<ThreadStart> TickTasks = new List<ThreadStart>();
        public float DefaultTickTime = 0.1F;
        public bool Enabled = true;
        float TickTime;

        /// <summary>
        /// Start the Ticker.
        /// Gets called when the class enables.
        /// </summary>
        void Start()
        {
            TickTime = DefaultTickTime;
        }

        /// <summary>
        /// Move to the next frame.
        /// Gets called once every frame.
        /// </summary>
        void Update()
        {
            if (Enabled)
            {
                TickTime -= Time.deltaTime;

                if (TickTime <= 0)
                {
                    foreach (ThreadStart Task in TickTasks)
                    {
                        Task.Invoke();
                    }
                    TickTime = DefaultTickTime;
                }
            }
        }

        /// <summary>
        /// Add a task to execute every tick.
        /// </summary>
        /// <param name="Task">Task to execute</param>
        public void AddTickTask(ThreadStart Task)
        {
            TickTasks.Add(Task);
        }
    }
}
