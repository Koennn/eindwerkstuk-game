﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Koen
{
    /// <summary>
    /// Helper object to store the current level.
    /// Can load a certain level or navigate to the titlescreen.
    /// </summary>
    class LevelController : MonoBehaviour
    {
        public int CurrentLevel = 0;

        /// <summary>
        /// Gets called every frame.
        /// </summary>
        void Update()
        {
            //Handle the EscapeMenu overlay.
            if (Cursor.visible && !SceneManager.GetActiveScene().name.Equals("TitleScreen"))
            {
                OverlayHelper.ShowEscapeMenu();
            }
        }

        /// <summary>
        /// Load a certain level using its Id.
        /// </summary>
        /// <param name="LevelId">Id of the level</param>
        public void LoadLevel(int LevelId)
        {
            if (SceneManager.GetSceneByName("LoadLevel" + LevelId) != null)
            {
                SceneManager.LoadScene("LoadLevel" + LevelId);
                CurrentLevel = LevelId;
            }
        }

        /// <summary>
        /// Load the next level using its id.
        /// </summary>
        public void LoadNextLevel()
        {
            if (SceneManager.GetSceneByName("LoadLevel" + CurrentLevel + 1) != null)
            {
                SceneManager.LoadScene("LoadLevel" + CurrentLevel + 1);
                CurrentLevel = CurrentLevel + 1;
            }

        }

        /// <summary>
        /// Load the TitleScreen.
        /// </summary>
        public void LoadTitleScreen()
        {
            SceneManager.LoadScene("TitleScreen");
            CurrentLevel = 0;
        }
    }
}
