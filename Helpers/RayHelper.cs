﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Helper class for Raycasting.
    /// </summary>
    public class RayHelper
    {
        /// <summary>
        /// Cast a ray from a Camera.
        /// </summary>
        /// <param name="Caster">Camera to cast from</param>
        /// <param name="MaxDistance">Maximum casting distance</param>
        /// <returns>GameObject first object in line of Camera</returns>
        public static GameObject Raycast(Camera Caster, float MaxDistance)
        {
            RaycastHit HitInfo;
            Ray ray = new Ray(Caster.transform.position, Caster.transform.forward);
            if (Physics.Raycast(ray, out HitInfo, MaxDistance))
            {
                return HitInfo.collider.gameObject;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Cast a ray from a GameObject.
        /// </summary>
        /// <param name="Caster">GameObject to cast from</param>
        /// <param name="MaxDistance">Maximum casting distance</param>
        /// <returns>GameObject first object in line of GameObject</returns>
        public static GameObject Raycast(GameObject Caster, float MaxDistance)
        {
            RaycastHit HitInfo;
            Ray ray = new Ray(Caster.transform.position, Caster.transform.forward);
            if (Physics.Raycast(ray, out HitInfo, MaxDistance))
            {
                return HitInfo.collider.gameObject;
            }
            else
            {
                return null;
            }
        }
    }
}
