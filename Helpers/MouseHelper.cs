﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Helper class for Mouse and Cursor related things.
    /// </summary>
    public class MouseHelper
    {

        /// <summary>
        /// Lock the mouse cursor to the center of the screen.
        /// </summary>
        public static void LockMouse()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.SetCursor(null, new Vector2(0, 0), CursorMode.Auto);
        }

        /// <summary>
        /// Unlock the mouse cursor and move it to the center of the screen.
        /// </summary>
        public static void UnlockMouse()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.SetCursor(null, new Vector2(0, 0), CursorMode.Auto);
        }
    }
}
