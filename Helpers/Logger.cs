﻿using System;

namespace Koen
{
    public class Logger
    {
        public static void Log(object msg)
        {
            Console.WriteLine(msg);
        }

        public static void Debug(object msg)
        {
            UnityEngine.Debug.Log(msg);
        }
    }
}
