﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Helper class to communicate with the keyboard and the mouse.
    /// Settings can be changed in the inspector.
    /// </summary>
    public class Controls : MonoBehaviour
    {
        public float JumpHeight = 4.0F;
        public float MovementSpeed = 8.0F;
        public float RotationSpeed = 3.0F;
        public float PitchRange = 60.0F;

        /// <summary>
        /// Get the Movement vector from the currently pressed keys (w, a, s, d).
        /// </summary>
        /// <returns>Vector3 Movement vector</returns>
        public Vector3 MovementVector()
        {
            float xMovement = Input.GetAxis("Horizontal") * MovementSpeed * 0.5F;
            float zMovement = Input.GetAxis("Vertical") * MovementSpeed;

            return new Vector3(xMovement, 0.0F, zMovement);
        }

        /// <summary>
        /// Get the current rotation yaw and pitch of the mouse.
        /// </summary>
        /// <param name="Yaw">Yaw of the mouse</param>
        /// <param name="Pitch">Pitch of the mouse</param>
        public void MouseRotation(out float Yaw, out float Pitch)
        {
            Yaw = Input.GetAxis("Mouse X") * RotationSpeed;
            Pitch = Input.GetAxis("Mouse Y") * RotationSpeed;
        }

        /// <summary>
        /// Check if the Jump key is pressed.
        /// </summary>
        /// <returns>boolean pressed</returns>
        public bool Jump(CharacterController Controller)
        {
            return Controller.isGrounded && Input.GetButton("Jump");
        }

        /// <summary>
        /// Check if the PrimaryAbility key is pressed.
        /// </summary>
        /// <returns>boolean pressed</returns>
        public bool PrimaryAbility()
        {
            return Input.GetButtonDown("Fire1");
        }

        /// <summary>
        /// Check if the SecondairyAbility key is pressed.
        /// </summary>
        /// <returns>boolean pressed</returns>
        public bool SecondairyAbility()
        {
            return Input.GetButtonDown("Fire2");
        }

        /// <summary>
        /// Check if the Escape key is pressed.
        /// </summary>
        /// <returns>boolean pressed</returns>
        public bool Escape()
        {
            return Input.GetButtonDown("Cancel");
        }

        /// <summary>
        /// Check if the Crouch key is being held down.
        /// </summary>
        /// <returns>boolean held</returns>
        public bool Crouch()
        {
            return Input.GetButton("Crouch");
        }

        /// <summary>
        /// Check if the Reload key is pressed.
        /// </summary>
        /// <returns>boolean pressed</returns>
        public bool Reload()
        {
            return Input.GetButtonDown("Reload");
        }
    }
}
