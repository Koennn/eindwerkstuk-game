﻿using System;
using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Custom class for Physics and movement control.
    /// </summary>
    public class CustomPhysics
    {
        /// <summary>
        /// Apply gravity to a CharacterController.
        /// </summary>
        /// <param name="Controller">CharacterController to apply gravity to</param>
        /// <param name="VerticalVelocity">VerticalVelocity of the CharacterController</param>
        [Obsolete("Attatch a PhysicsComponent instead!")]
        public static void ApplyGravity(CharacterController Controller, ref float VerticalVelocity)
        {
            if (!Controller.isGrounded)
            {
                VerticalVelocity += Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                VerticalVelocity = VerticalVelocity < 1 ? -0.0001F : VerticalVelocity;
            }
            Controller.Move(new Vector3(0, VerticalVelocity, 0) * Time.deltaTime);
        }

        /// <summary>
        /// Move a CharacterController in a certain direction (Vector3).
        /// </summary>
        /// <param name="Controller">CharacterController to move</param>
        /// <param name="Direction">Direction to move it in (Vector3)</param>
        /// <param name="Rotation">Rotation of the CharacterController</param>
        /// <param name="VerticalVelocity">VerticalVelocity of the CharacterController</param>
        [Obsolete("Use GetComponent<PhysicsComponent>().Move instead!")]
        public static void Move(CharacterController Controller, Vector3 Direction, Quaternion Rotation,ref float VerticalVelocity)
        {
            if (!Controller.isGrounded)
            {
                Direction *= 0.5F;
                VerticalVelocity += Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                VerticalVelocity = VerticalVelocity < 1 ? -0.0001F : VerticalVelocity;
            }
            Vector3 Movement = new Vector3(Direction.x, VerticalVelocity, Direction.z);
            Movement = Rotation * Movement;
            Controller.Move(Movement * Time.deltaTime);
        }

        /// <summary>
        /// Make the CharacterController jump up in the air.
        /// </summary>
        /// <param name="Controller">CharacterController to use</param>
        /// <param name="Rotation">Rotation of the CharacterController</param>
        /// <param name="VerticalVelocity">VerticalVelocity of the CharacterController</param>
        [Obsolete("Use GetComponent<PhysicsComponent>().Jump instead!")]
        public static void Jump(CharacterController Controller, Quaternion Rotation, ref float VerticalVelocity)
        {
            VerticalVelocity = Game.Controls.JumpHeight;
            Move(Controller, new Vector3(0, 0, 0), Rotation, ref VerticalVelocity);
        }

        /// <summary>
        /// Move a GameObject to a certain position with a certain rotation.
        /// </summary>
        /// <param name="Object">GameObject to move</param>
        /// <param name="Position">Position to move it to</param>
        /// <param name="Rotation">Rotation to give it</param>
        [Obsolete("Use GetComponent<PhysicsComponent>().Move instead!")]
        public static void MoveTo(GameObject Object, Vector3 Position, Quaternion Rotation)
        {
            Object.transform.position = Position;
            Object.transform.rotation = Rotation;
        }
    }
}
