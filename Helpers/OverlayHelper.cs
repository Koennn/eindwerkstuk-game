﻿using System;
using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Helper class for all Overlay related things.
    /// </summary>
    public class OverlayHelper
    {
        /// <summary>
        /// Is true when the EscapeMenu is shown.
        /// </summary>
        public static bool EscapeMenu;

        /// <summary>
        /// Show the Escape menu overlay.
        /// </summary>
        public static void ShowEscapeMenu()
        {
            Game.EscapeOverlay.gameObject.SetActive(true);
            MouseHelper.UnlockMouse();
            EscapeMenu = true;
        }

        /// <summary>
        /// Hide the Escape menu overlay.
        /// </summary>
        public static void HideEscapeMenu()
        {
            Game.EscapeOverlay.gameObject.SetActive(false);
            MouseHelper.LockMouse();
            EscapeMenu = false;
        }

        /// <summary>
        /// Show the QuitOverlay.
        /// </summary>
        public static void ShowQuitOverlay()
        {
            Game.QuitOverlay.gameObject.SetActive(true);
            MouseHelper.UnlockMouse();
        }
    }
}
