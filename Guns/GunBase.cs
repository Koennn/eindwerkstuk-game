﻿using System;
using System.Collections;
using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Base class for all guns.
    /// Does nothing on its own, needs to be extended.
    /// </summary>
    public abstract class GunBase : MonoBehaviour
    {
        private WeaponManager Manager;
        private Animation Animations;

        /// <summary>
        /// Name of the gun.
        /// </summary>
        public string Name;

        /// <summary>
        /// Prefab of the gun's model.
        /// </summary>
        public GameObject Prefab;

        /// <summary>
        /// Maximum amount of ammo.
        /// </summary>
        public int MaxAmmo;
        
        /// <summary>
        /// Reload time of the gun.
        /// </summary>
        public int ReloadTime;

        /// <summary>
        /// Maximum range of the gun.
        /// </summary>
        public float Range;

        /// <summary>
        /// Stats of the gun.
        /// </summary>
        public GunStats Stats;

        /// <summary>
        /// Current amount of ammo.
        /// </summary>
        public int CurrentAmmo;

        /// <summary>
        /// Is true if the gun is currently reloading.
        /// </summary>
        public bool Reloading;

        /// <summary>
        /// GunBase constructor.
        /// Sets all importand values and loads animations.
        /// </summary>
        /// <param name="Name">Name of the gun</param>
        /// <param name="MaxAmmo">Maximum amount of ammo for the gun</param>
        /// <param name="ReloadTime">Reload time of the gun</param>
        public GunBase(string Name, int MaxAmmo, int ReloadTime)
        {
            Manager = Game.PlayerObject.gameObject.GetComponent<WeaponManager>();
            Prefab = Manager.GetGun(Manager.SelectedGun);
            Animations = Prefab.GetComponent<Animation>();
            this.Name = Name;
            this.MaxAmmo = MaxAmmo;
            this.ReloadTime = ReloadTime;
        }

        /// <summary>
        /// Get called whenever the bullet hits a Collider.
        /// </summary>
        /// <param name="Other">Collider which gets hit</param>
        public virtual void OnCollide(Collider Other)
        {
        }

        /// <summary>
        /// Gets called whenever the bullet hits an Entity.
        /// </summary>
        /// <param name="Entity">Entity which gets hit</param>
        public abstract void OnHit(GameObject Entity, HealthController Health);

        /// <summary>
        /// Fire the gun.
        /// Can be overridden to change shooting behaviour.
        /// </summary>
        /// <param name="Caster">Camera to Cast the Ray from</param>
        public virtual void Fire(Camera Caster)
        {
            //Check if the gun is out of ammo.
            if (CurrentAmmo <= 0)
            {
                //Automaticly start relaoding the gun.
                StartReload();

                //Cancel the shooting.
                return;
            }

            //Check if the gun is currently reloading.
            if (Reloading)
            {
                //Cancel the shooting.
                return;
            }

            //Make the Raycast and get the GameObject that gets hit.
            GameObject Hit = RayHelper.Raycast(Caster, Range);
            
            //Check if anything got hit at all.
            if (Hit != null)
            {
                //Call the OnCollide method and pass in the Collider.
                OnCollide(Hit.GetComponent<Collider>());

                //Check if the GameObject contains a HealthController component.
                if (Hit.GetComponent<HealthController>() != null)
                {
                    //Call the OnHit method and pass in the GameObject.
                    OnHit(Hit, Hit.GetComponent<HealthController>());
                }
            }

            //Try to play the ShootAnimation.
            try
            {
                Animations.Play("ShootAnimation");
            }
            catch (Exception)
            {
                Debug.Log("No Shooting animation found!");
            }
        }

        /// <summary>
        /// Start reloading the gun.
        /// Automaticly starts the ReloadAnimation.
        /// </summary>
        public void StartReload()
        {
            //Try to play the ReloadAnimation.
            try
            {
                Animations.Play("ReloadAnimation");
            }
            catch (Exception)
            {
                Debug.Log("No Shooting animation found!");
            }

            //Set the Reloaing variable to true.
            Reloading = true;

            //Start the Reload Coroutine.
            StartCoroutine(Reload());
        }

        /// <summary>
        /// Reload the Gun to the MaxAmmo.
        /// </summary>
        /// <returns>IEnumerator ReloadTime</returns>
        private IEnumerator Reload()
        {
            //Return the ReloadTime.
            yield return ReloadTime;

            //Set the Reloading variable to false.
            Reloading = false;

            //Reset the CurrentAmmo to MaxAmmo.
            CurrentAmmo = MaxAmmo;
        }
    }
}
