﻿namespace Koen
{
    /// <summary>
    /// All the different gun names.
    /// </summary>
    public enum Gun
    {
        MINIGUN, SHOTGUN, SNIPERGUN, ROCKETLAUNCHER, SMG, I69
    }
}