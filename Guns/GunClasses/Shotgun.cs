﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// GunClass for the Shotgun.
    /// </summary>
    class Shotgun : GunBase
    {
        public Shotgun() : base("Shotgun", 4, 3)
        {
            Stats = new GunStats(this, 4, 3, 8);
        }

        /// <summary>
        /// Apply damage to the hit entity.
        /// </summary>
        /// <param name="Entity">Entity who gets hit</param>
        /// <param name="Health">HealthController of the Entity</param>
        public override void OnHit(GameObject Entity, HealthController Health)
        {
            Health.ApplyDamage(Stats.BulletDamage);
        }
    }
}
