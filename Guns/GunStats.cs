﻿namespace Koen
{
    /// <summary>
    /// All the stats of the GunBase.
    /// </summary>
    public class GunStats
    {
        public GunBase Base;
        public int MaxAmmo;
        public int ReloadTime;
        public int BulletDamage;

        public GunStats(GunBase Base, int MaxAmmo, int ReloadTime, int BulletDamage)
        {
            this.Base = Base;
            this.MaxAmmo = MaxAmmo;
            this.ReloadTime = ReloadTime;
            this.BulletDamage = BulletDamage;
        }
    }
}
