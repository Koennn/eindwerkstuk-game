﻿using UnityEngine;
using System.Collections.Generic;

namespace Koen
{
    /// <summary>
    /// Manager class for equipped weapons.
    /// Weapon prefabs and selected weapon can be selected in the inspector.
    /// </summary>
    public class WeaponManager : MonoBehaviour
    {
        public List<GameObject> GunPrefabs = new List<GameObject>();
        public Gun SelectedGun;
        public GunBase GunClass;
        private GameObject GunSlot;
        private GameObject GunObject;

        /// <summary>
        /// Initialize the GunSlot and select the first weapon.
        /// </summary>
        void Start()
        {
            GunSlot = GameObject.Find("GunSlot");
            InitGunObject(SelectedGun);
        }

        /// <summary>
        /// Switch to the SelectedWeapon if it changed.
        /// Gets called every frame.
        /// </summary>
        void Update()
        {
            if (!GunObject.name.Equals(SelectedGun.ToString()))
            {
                InitGunObject(SelectedGun);
            }
            if (GunClass != null)
            {
                if (Game.Controls.PrimaryAbility())
                {
                    GunClass.Fire(Camera.main);
                }
                if (Game.Controls.Reload())
                {
                    GunClass.StartReload();
                }
            }
        }

        /// <summary>
        /// Get a GameObject from the Gun enum.
        /// </summary>
        /// <param name="Gun">Gun enum value</param>
        /// <returns>GameObject gun prefab</returns>
        public GameObject GetGun(Gun Gun)
        {
            foreach (GameObject GunObject in GunPrefabs)
            {
                if (GunObject.name.Equals(Gun.ToString()))
                {
                    return GunObject;
                }
            }
            return null;
        }

        /// <summary>
        /// Initialize a Gun into a GameObject.
        /// </summary>
        /// <param name="Gun">Gun to initialize</param>
        void InitGunObject(Gun Gun)
        {
            GunObject = Instantiate(GetGun(Gun));
            GunObject.name = Gun.ToString();
            GunObject.transform.parent = GunSlot.transform;
            GunObject.transform.localPosition = new Vector3(0, 0, 0);
            GunObject.transform.Rotate(0, 90, 0);
            GunObject.layer = 9;
            Transform[] allChildren = GunObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                child.gameObject.layer = 9;
            }
        }
    }
}
