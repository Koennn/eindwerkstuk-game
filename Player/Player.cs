﻿using UnityEngine;

namespace Koen
{
    /// <summary>
    /// Player object class.
    /// This is the FirstPerson player, and has nothing to do with the OnlinePlayer.
    /// The object gets instanstiated once when the game loads up.
    /// </summary>
    public class Player : MonoBehaviour
    {
        private CharacterController cc;
        private float CurrentPitchRotation = 0.0F;
        private float VerticalVelocity = 0.0F;
        private CrouchController Crouch;
        private PhysicsComponent Phys;

        /// <summary>
        /// Initialize the components and setup TickTasks.
        /// Gets called when the object gets created.
        /// </summary>
        public void Start()
        {
            //Get the CharacterController component.
            cc = GetComponent<CharacterController>();
            Crouch = GetComponent<CrouchController>();
            Phys = GetComponent<PhysicsComponent>();
        }
        
        /// <summary>
        /// Handles all movement and interaction related events.
        /// Gets called every frame.
        /// </summary>
        void Update()
        {
            //Make a SpeedReduction variable.
            float SpeedReduction = 1.0F;

            //Check if the EscapeMenu overlay is enabled.
            if (OverlayHelper.EscapeMenu)
            {
                //Apply gravity to the player, and move on to the next frame.
                //CustomPhysics.ApplyGravity(cc, ref VerticalVelocity);
                return;
            }

            //Yaw and pitch variables.
            float Yaw;
            float Pitch;

            //Get the current rotation Yaw and Pitch.
            Game.Controls.MouseRotation(out Yaw, out Pitch);

            //Rotate the Yaw.
            transform.Rotate(0, Yaw, 0);

            //Calculate the Pitch and make sure it is within the bounds.
            CurrentPitchRotation -= Pitch;
            CurrentPitchRotation = Mathf.Clamp(CurrentPitchRotation, -Game.Controls.PitchRange, Game.Controls.PitchRange);

            //Rotate the Pitch.
            Camera.main.transform.rotation = Quaternion.Euler(CurrentPitchRotation, Camera.main.transform.rotation.eulerAngles.y, Camera.main.transform.rotation.eulerAngles.z);

            //Handle jumping.
            if (Game.Controls.Jump(cc) && !Crouch.IsCrouched)
            {
                Phys.Jump();
            }

            //Check if the player is Crouching.
            if (Crouch.IsCrouched)
            {
                SpeedReduction = 0.3F;
            }

            //Move the player.
            Phys.Move(transform.rotation * (Game.Controls.MovementVector() * SpeedReduction));
        }
    }
}
