﻿using System;
using UnityEngine;

namespace Koen
{
    public class CrouchController : MonoBehaviour
    {
        public bool IsCrouched = false;
        private CharacterController Controller;

        void Start()
        {
            Controller = GetComponent<CharacterController>();
        }

        void Update()
        {
            if (Game.Controls.Crouch())
            {
                if (Controller.height != 1.1F)
                {
                    Controller.height = 1.1F;
                    Vector3 CameraPos = Camera.main.transform.localPosition;
                    Camera.main.transform.localPosition = new Vector3(CameraPos.x, CameraPos.y - 0.4F, CameraPos.z);
                    IsCrouched = true;
                }
                    
            } else
            {
                if (Controller.height == 1.1F)
                {
                    Controller.height = 2.0F;
                    Vector3 CameraPos = Camera.main.transform.localPosition;
                    Camera.main.transform.localPosition = new Vector3(CameraPos.x, CameraPos.y + 0.4F, CameraPos.z);
                    IsCrouched = false;
                }
            }
        }
    }
}
